import javax.swing.JFrame;

/*
 * 資管4A
 * 105403031  莫智堯
 */

public class Main extends HW1_105403031 {

    public static void main(String[] args) {
        HW1_105403031 HW1_drawer = new HW1_105403031();
        HW1_drawer.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        HW1_drawer.setSize(700, 400);
        HW1_drawer.setVisible(true);

    }
}